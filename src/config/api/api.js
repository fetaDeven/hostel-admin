import config from "../config";

const loginUser = async (body) => {
  try {
    const response = await fetch(`${config.serverUrl}/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    });

    const res = await response.json();
    if (res.server.status) {
      return { success: true, ...res };
    } else {
      return { success: false, message: res.message };
    }
  } catch (error) {
    return { error: error.message };
  }
};

const getHostels = async (id) => {
  try {
    const response = await fetch(`${config.serverUrl}/hostels`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        id,
      },
    });

    const res = await response.json();
    if (res.server.status) {
      return { success: true, ...res };
    } else {
      return { success: false, message: res.message };
    }
  } catch (error) {
    return { error: error.message };
  }
};

const getBookings = async (hostelId) => {
  try {
    const response = await fetch(
      `${config.serverUrl}/booking?hostelId=${hostelId}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    const res = await response.json();
    if (res.server.status) {
      return { success: true, ...res };
    } else {
      return { success: false, message: res.message };
    }
  } catch (error) {
    return { error: error.message };
  }
};

const rejectStudentBooking = async (bookingId, body) => {
  try {
    const response = await fetch(`${config.serverUrl}/booking/${bookingId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    });

    const res = await response.json();
    if (res.server.status) {
      return { success: true, ...res };
    } else {
      return { success: false, message: res.message };
    }
  } catch (error) {
    return { error: error.message };
  }
};

const acceptStudentBooking = async (bookingId, body) => {
  try {
    const response = await fetch(
      `${config.serverUrl}/booking/accept/${bookingId}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      }
    );

    const res = await response.json();
    if (res.server.status) {
      return { success: true, ...res };
    } else {
      return { success: false, message: res.message };
    }
  } catch (error) {
    return { error: error.message };
  }
};

// accepted-bookings
const getAcceptedBookings = async (hostelId) => {
  try {
    const response = await fetch(
      `${config.serverUrl}/booking/accepted-bookings?hostelId=${hostelId}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    const res = await response.json();
    if (res.server.status) {
      return { success: true, ...res };
    } else {
      return { success: false, message: res.message };
    }
  } catch (error) {
    return { error: error.message };
  }
};

const acceptStudentPayment = async (id, body) => {
  try {
    const response = await fetch(`${config.serverUrl}/booking/payment/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    });

    const res = await response.json();
    if (res.server.status) {
      return { success: true, ...res };
    } else {
      return { success: false, message: res.message };
    }
  } catch (error) {
    return { error: error.message };
  }
};

const getMyHostelDetails = async (id) => {
  try {
    const response = await fetch(`${config.serverUrl}/hostels/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    const res = await response.json();
    if (res.server.status) {
      return { success: true, ...res };
    } else {
      return { success: false, message: res.message };
    }
  } catch (error) {
    return { error: error.message };
  }
};

const addMyHostel = async (body, id) => {
  try {
    const response = await fetch(`${config.serverUrl}/hostels`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        id,
      },
      body: JSON.stringify(body),
    });

    const res = await response.json();
    if (res.server.status) {
      return { success: true, ...res };
    } else {
      return { success: false, message: res.message };
    }
  } catch (error) {
    return { error: error.message };
  }
};

const addMyFloors = async (body, id) => {
  try {
    const response = await fetch(`${config.serverUrl}/floors`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        id,
      },
      body: JSON.stringify(body),
    });

    const res = await response.json();
    if (res.server.status) {
      return { success: true, ...res };
    } else {
      return { success: false, message: res.message };
    }
  } catch (error) {
    return { error: error.message };
  }
};

const addMyRooms = async (body, id) => {
  try {
    const response = await fetch(`${config.serverUrl}/rooms`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        id,
      },
      body: JSON.stringify(body),
    });

    const res = await response.json();
    if (res.server.status) {
      return { success: true, ...res };
    } else {
      return { success: false, message: res.message };
    }
  } catch (error) {
    return { error: error.message };
  }
};

const addImage = async (body, id) => {
  try {
    const response = await fetch(`${config.serverUrl}/images/${id}`, {
      method: "POST",
      body,
    });

    const res = await response.json();
    if (res.server.status) {
      return { success: true, ...res };
    } else {
      return { success: false, message: res.message };
    }
  } catch (error) {
    return { error: error.message };
  }
};

export {
  loginUser,
  getHostels,
  getBookings,
  rejectStudentBooking,
  acceptStudentBooking,
  getAcceptedBookings,
  acceptStudentPayment,
  getMyHostelDetails,
  addMyHostel,
  addMyFloors,
  addMyRooms,
  addImage,
};
