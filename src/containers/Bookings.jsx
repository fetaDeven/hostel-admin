import { Button, Form, message, Select } from "antd";
import React, { useState } from "react";
import AcceptBooking from "../components/booking/AcceptBooking";
import BookingTable from "../components/booking/BookingTable";
import MainContent from "../components/shared/MainContent";
import {
  acceptStudentBooking,
  getBookings,
  rejectStudentBooking,
} from "../config/api/api";
import { getStorage, setStorage } from "../config/storage/storage";

const Booking = () => {
  const [loading, setLoading] = useState(false);
  const [acceptModal, setAcceptModal] = useState(false);

  const hostels = getStorage("hostels");

  const rejectBooking = async (id) => {
    message.loading("Rejecting booking", 3);
    const reject = await rejectStudentBooking(id, { status: "Rejected" });
    if (reject.success) {
      message.success("Booking rejected successfully");
    } else {
      message.error(reject.message);
    }
  };
  const acceptBooking = async (details) => {
    message.loading("Loading", 1);
    const accept = await acceptStudentBooking(details.id, {
      user_id: details?.user_id,
      room_id: details?.room_id,
    });
    if (accept.success) {
      message.success("Booking accepted successfully");
      setAcceptModal(false);
    } else {
      message.error(accept.message);
    }
  };
  const closeModal = () => {
    setAcceptModal(false);
  };

  const onFinish = async (values) => {
    setLoading(true);
    const bookings = await getBookings(values.hostelId);

    if (bookings.success) {
      setStorage("bookings", bookings);
    } else {
      message.error(bookings.message);
    }
    setLoading(false);
  };
  return (
    <>
      <h3 className="fw-light m-3">Room Bookings</h3>
      <MainContent>
        <div className="w-75">
          <Form onFinish={onFinish} layout="horizontal">
            <div className="d-flex">
              <Form.Item
                rules={[{ required: true, message: "Please select a hostel" }]}
                className="w-75 mx-2"
                name={"hostelId"}
              >
                <Select placeholder="Select Hostel">
                  {hostels?.userHostels?.map((hostel) => (
                    <Select.Option value={hostel.id} key={hostel.id}>
                      {hostel.name}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
              <Button loading={loading} type="primary" htmlType="submit">
                Search
              </Button>
            </div>
          </Form>
        </div>
        <BookingTable
          setBookingDetails={acceptBooking}
          rejectBooking={rejectBooking}
          loading={loading}
        />
        <AcceptBooking
          visible={acceptModal}
          closeModal={closeModal}
          acceptBooking={acceptBooking}
        />
      </MainContent>
    </>
  );
};

export default Booking;
