import { Button, Layout, Result } from "antd";
import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Login from "./containers/Login";
import MainLayout from "./layouts/MainLayout";

const App = () => {
  const goHome = () => {
    window.location.href = "/";
  };

  return (
    <Layout
      style={{
        minHeight: "100vh",
      }}
    >
      <BrowserRouter>
        <Routes>
          <Route path="/*" element={<MainLayout />} />
          <Route path="/login" element={<Login />} />
          <Route
            path="*"
            element={
              <Result
                status="404"
                title="404"
                subTitle="Sorry, the page you visited does not exist."
                extra={
                  <Button onClick={goHome} type="primary">
                    Back Home
                  </Button>
                }
              />
            }
          ></Route>
        </Routes>
      </BrowserRouter>
    </Layout>
  );
};

export default App;
