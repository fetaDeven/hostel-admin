import { Layout } from "antd";
import React from "react";

const { Content } = Layout;
const MainContent = ({ children }) => {
  return (
    <Content
      style={{
        margin: "0 16px",
      }}
    >
      <br />
      <div
        className="site-layout-background"
        style={{
          padding: 24,
          minHeight: 360,
        }}
      >
        {children}
      </div>
    </Content>
  );
};

export default MainContent;
