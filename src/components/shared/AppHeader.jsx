import { Layout } from "antd";
import React from "react";
import { AiOutlineUser } from "react-icons/ai";
import { getStorage } from "../../config/storage/storage";

const { Header } = Layout;
const AppHeader = () => {
  const user = getStorage("token");
  return (
    <Header
      className="site-layout-background d-flex align-items-center px-3"
      style={{
        padding: 0,
      }}
    >
      <div className="w-50">
        <h3 className="fw-light">Admin Portal</h3>
      </div>
      <div className="w-50 d-flex justify-content-end align-items-center">
        <p className="m-3 text-primary">{user?.username}</p>
        <h3 className="">
          <AiOutlineUser />
        </h3>
      </div>
    </Header>
  );
};

export default AppHeader;
