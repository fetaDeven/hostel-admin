import { Menu } from "antd";
import React from "react";
import {
  AiOutlineDesktop,
  AiOutlineHome,
  AiOutlineUser,
  AiOutlineLogout,
  AiOutlinePayCircle,
} from "react-icons/ai";
import { Link, useNavigate } from "react-router-dom";
import { removeStorage } from "../../config/storage/storage";

const AppMenu = () => {
  const navigate = useNavigate();
  const logOut = () => {
    removeStorage("token");
    navigate("/login");
  };
  return (
    <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
      <Menu.Item key={1} icon={<AiOutlineDesktop />}>
        <Link to={"/"}>
          <span>Home</span>
        </Link>
      </Menu.Item>
      <Menu.Item key={2} icon={<AiOutlineHome />}>
        <Link to={"/booking"}>
          <span>Room Bookings</span>
        </Link>
      </Menu.Item>
      <Menu.Item key={3} icon={<AiOutlinePayCircle />}>
        <Link to={"/payments"}>
          <span>Payments</span>
        </Link>
      </Menu.Item>
      <Menu.Item key={4} icon={<AiOutlineUser />}>
        <Link to={"/profile"}>
          <span>User Profile</span>
        </Link>
      </Menu.Item>
      <Menu.Item onClick={logOut} key={5} icon={<AiOutlineLogout />}>
        <span>Logout</span>
      </Menu.Item>
    </Menu>
  );
};

export default AppMenu;
