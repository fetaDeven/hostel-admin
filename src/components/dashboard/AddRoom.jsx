import { Button, Form, Input, InputNumber, Modal, Select } from "antd";
import React from "react";
import { getStorage } from "../../config/storage/storage";

const AddRoom = ({ visible, add, closeModal }) => {
  const hostelDetails = getStorage("hostel_details");
  return (
    <Modal
      open={visible}
      onCancel={closeModal}
      closable
      footer={null}
      title="Add Room"
    >
      <Form layout="vertical" onFinish={add}>
        <Form.Item
          name={"name"}
          label="Name"
          rules={[{ required: true, message: "Please select an end date" }]}
        >
          <Input className="w-100" />
        </Form.Item>
        <Form.Item
          name={"price"}
          label="Price"
          rules={[{ required: true, message: "Please select an price" }]}
        >
          <InputNumber className="w-100" />
        </Form.Item>
        <Form.Item
          name={"total_occupants"}
          label="Capacity"
          rules={[{ required: true, message: "Please select capacity" }]}
        >
          <InputNumber className="w-100" />
        </Form.Item>
        <Form.Item
          name={"floor_id"}
          label="Floor"
          rules={[{ required: true, message: "Please select an end date" }]}
        >
          <Select>
            {hostelDetails?.floors?.map((floor) => (
              <Select.Option key={floor.id} value={floor.id}>
                {floor.name}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Button type="primary" htmlType="submit">
          Add
        </Button>
      </Form>
    </Modal>
  );
};

export default AddRoom;
