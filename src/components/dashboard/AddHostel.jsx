import { Button, Form, Input, Modal } from "antd";
import React from "react";

const AddHostel = ({ visible, closeModal, addHostel }) => {
  return (
    <Modal
      open={visible}
      onCancel={closeModal}
      closable
      footer={null}
      title="Add Hostel"
    >
      <Form layout="vertical" onFinish={addHostel}>
        <Form.Item
          name={"name"}
          label="Name"
          rules={[{ required: true, message: "Please select an end date" }]}
        >
          <Input className="w-100" />
        </Form.Item>
        <Form.Item
          name={"contact"}
          label="Contact"
          rules={[{ required: true, message: "Please select an end date" }]}
        >
          <Input className="w-100" />
        </Form.Item>
        <Form.Item
          name={"location"}
          label="Location"
          rules={[{ required: true, message: "Please select an end date" }]}
        >
          <Input.TextArea className="w-100" />
        </Form.Item>
        <Button type="primary" htmlType="submit">
          Add
        </Button>
      </Form>
    </Modal>
  );
};

export default AddHostel;
