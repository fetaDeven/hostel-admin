import { Button, Form, Input, Modal, Select } from "antd";
import React from "react";

const AddFloor = ({ visible, add, closeModal }) => {
  return (
    <Modal
      open={visible}
      onCancel={closeModal}
      closable
      footer={null}
      title="Add Floor"
    >
      <Form layout="vertical" onFinish={add}>
        <Form.Item
          name={"name"}
          label="Name"
          rules={[{ required: true, message: "Please select an end date" }]}
        >
          <Input className="w-100" />
        </Form.Item>
        <Form.Item
          name={"gender"}
          label="Gender"
          rules={[{ required: true, message: "Please select an end date" }]}
        >
          <Select allowClear>
            <Select.Option value="Male">Male</Select.Option>
            <Select.Option value="Female">Female</Select.Option>
            <Select.Option value="Mixed">Mixed</Select.Option>
          </Select>
        </Form.Item>
        <Button type="primary" htmlType="submit">
          Add
        </Button>
      </Form>
    </Modal>
  );
};

export default AddFloor;
