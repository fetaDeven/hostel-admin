import { Button, Form, Modal, Select, Upload } from "antd";
import React from "react";
import { InboxOutlined } from "@ant-design/icons";
import { getStorage } from "../../config/storage/storage";

const AddRoomImage = ({ visible, onClose, addImage }) => {
  const hostelDetails = getStorage("hostel_details");
  return (
    <Modal
      open={visible}
      onCancel={onClose}
      closable
      footer={null}
      title="Add Room Image"
    >
      <Form layout="vertical" onFinish={addImage}>
        <Form.Item
          name={"id"}
          label="Room"
          rules={[{ required: true, message: "Please select an end date" }]}
        >
          <Select>
            {hostelDetails?.rooms?.map((floor) => (
              <Select.Option key={floor.id} value={floor.id}>
                {floor.name}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          name="image"
          rules={[{ required: true, message: "Please add an Image" }]}
        >
          <Upload.Dragger name="file" listType={null} maxCount={1}>
            <p className="ant-upload-drag-icon">
              <InboxOutlined />
            </p>
            <p className="ant-upload-text">
              Click or drag Image to this area to upload
            </p>
          </Upload.Dragger>
        </Form.Item>
        <Button type="primary" htmlType="submit">
          Add
        </Button>
      </Form>
    </Modal>
  );
};

export default AddRoomImage;
