import { Button, Form, InputNumber, Modal } from "antd";
import React from "react";

const PaymentForm = ({ visible, closeModal, acceptPayment }) => {
  return (
    <div>
      <Modal
        open={visible}
        onCancel={closeModal}
        closable
        footer={null}
        title="Accept Books"
      >
        <Form layout="vertical" onFinish={acceptPayment}>
          <Form.Item
            name={"amount"}
            label="Amount"
            rules={[{ required: true, message: "Please enter an ammount" }]}
          >
            <InputNumber className="w-100" />
          </Form.Item>
          <Button type="primary" htmlType="submit">
            Accept
          </Button>
        </Form>
      </Modal>
    </div>
  );
};

export default PaymentForm;
