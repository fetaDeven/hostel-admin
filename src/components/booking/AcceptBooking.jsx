import { Button, DatePicker, Form, Modal } from "antd";
import React from "react";

const AcceptBooking = ({ visible, closeModal, acceptBooking }) => {
  return (
    <div>
      <Modal
        open={visible}
        onCancel={closeModal}
        closable
        footer={null}
        title="Accept Books"
      >
        <Form layout="vertical" onFinish={acceptBooking}>
          <Form.Item
            name={"end_date"}
            label="End Date"
            rules={[{ required: true, message: "Please select an end date" }]}
          >
            <DatePicker className="w-100" />
          </Form.Item>
          <Button type="primary" htmlType="submit">
            Accept
          </Button>
        </Form>
      </Modal>
    </div>
  );
};

export default AcceptBooking;
