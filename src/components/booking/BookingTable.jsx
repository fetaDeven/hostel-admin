import { Button, Popover, Space, Table, Tag } from "antd";
import React from "react";
import { getStorage } from "../../config/storage/storage";

const BookingTable = ({ loading, setBookingDetails, rejectBooking }) => {
  const dataSource = getStorage("bookings");
  const columns = [
    {
      title: "Room",
      key: "name",
      render: (text) => text?.hostel_room?.name,
    },
    {
      title: "Student",
      render: (text) => text?.student?.name,
      key: "age",
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      render: (text) => `${text?.split("-")[0]}`,
    },
    {
      title: "Amount Paid",
      key: "AmmountPaid",
      render: (text) => `${text?.status?.split("-")[1] || "Not Paid"}`,
    },
    {
      title: "Start",
      dataIndex: "start_date",
      key: "age",
      render: (text) => (text ? new Date(text).toDateString() : "Not Set"),
    },
    {
      title: "End Date",
      dataIndex: "end_date",
      key: "age",
      render: (text) => (text ? new Date(text).toDateString() : "Not Set"),
    },
    {
      title: "Paid",
      dataIndex: "paid",
      key: "paid",
      render: (text) =>
        text ? <Tag color="success">Yes</Tag> : <Tag color="error">No</Tag>,
    },
    {
      title: "Actions",
      key: "address",
      render: (record) => {
        return (
          <Popover
            content={
              <Space direction="vertical">
                <Button
                  onClick={() =>
                    setBookingDetails({
                      user_id: record?.user_id,
                      room_id: record?.room_id,
                      id: record?.id,
                    })
                  }
                  className="bg-light"
                  type="dashed"
                >
                  Accept
                </Button>
                <Button
                  onClick={() => rejectBooking(record.id)}
                  type="dashed"
                  className="w-100"
                  danger
                >
                  Reject
                </Button>
              </Space>
            }
            placement="bottom"
          >
            <Button>Actions</Button>
          </Popover>
        );
      },
    },
  ];
  return (
    <div>
      <Table
        loading={loading}
        dataSource={dataSource?.booking}
        columns={columns}
      />
    </div>
  );
};

export default BookingTable;
