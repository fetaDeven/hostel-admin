import { Button, Layout, Result } from "antd";
import React, { useEffect, useState } from "react";
import { BankOutlined } from "@ant-design/icons";
import AppMenu from "../components/shared/AppMenu";
import AppHeader from "../components/shared/AppHeader";
import { Route, Routes } from "react-router-dom";
import Dashboard from "../containers/Dashboard";
import Booking from "../containers/Bookings";
import Payments from "../containers/Payments";
import Profile from "../containers/Profile";
import { getStorage } from "../config/storage/storage";

const { Footer, Sider } = Layout;
const MainLayout = () => {
  const [collapsed, setCollapsed] = useState(true);
  useEffect(() => {
    const token = getStorage("token");
    if (!token) {
      window.location.href = "/login";
    }
  }, []);

  const goHome = () => {
    window.location.href = "/";
  };
  return (
    <>
      <Sider
        collapsible
        collapsed={collapsed}
        onCollapse={(value) => setCollapsed(value)}
      >
        <div className="d-flex flex-column mb-3">
          <h3 className="text-light m-3 text-center">
            <BankOutlined color="#fff" />
          </h3>
          {!collapsed && (
            <h4 className="text-light text-center fw-light">E Hostel App</h4>
          )}
        </div>
        <AppMenu />
      </Sider>
      <Layout className="site-layout">
        <AppHeader />
        <Routes>
          <Route path="/" element={<Dashboard />} />
          <Route path="/booking" element={<Booking />} />
          <Route path="/payments" element={<Payments />} />
          <Route path="/profile" element={<Profile />} />
          <Route
            path="*"
            element={
              <Result
                status="404"
                title="404"
                subTitle="Sorry, the page you visited does not exist."
                extra={
                  <Button onClick={goHome} type="primary">
                    Back Home
                  </Button>
                }
              />
            }
          />
        </Routes>
        <Footer
          style={{
            textAlign: "center",
          }}
        >
          Group 28
        </Footer>
      </Layout>
    </>
  );
};

export default MainLayout;
